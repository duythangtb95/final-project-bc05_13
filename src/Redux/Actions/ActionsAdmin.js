import { GET_THONGTINKHOAHOC } from "../constants/adminsConstans"

export const ActionsAdmin = {

    getTTKhoaHocPopUp: (maKhoaHoc) => {
        return {
        type: GET_THONGTINKHOAHOC,
        payload: maKhoaHoc,
     }
    },
}