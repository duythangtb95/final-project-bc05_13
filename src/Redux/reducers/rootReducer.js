import { combineReducers } from "redux";
import { userReducer } from "./userReducer";
import { adminReducer } from "./adminReducer";

export const rootReducer = combineReducers({
  userReducer,
  adminReducer,
});
