import { userLocalService } from "../../Services/localStoreServices/localStoreServices";
import { SET_USER_INFOR } from "../constants/userConstants";

const initialState = {
  userInfo: userLocalService.get(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_INFOR:
      return { ...state, userInfo: payload };

    default:
      return state;
  }
};
