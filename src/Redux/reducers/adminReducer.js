import { adminServices } from '../../Services/AdminService/adminService'
import { GET_THONGTINKHOAHOC } from '../constants/adminsConstans';

const DataAdmin = {
  ThongTinKhoaHoc: {},
};

export const adminReducer = (state = DataAdmin, { type, payload }) => {
  switch (type) {
    case GET_THONGTINKHOAHOC: {
      let cloneThongTinKhoaHoc = { ...state.ThongTinKhoaHoc };

      let maKhoaHoc = payload;

      adminServices
        .getLayThongTinKhoaHoc(maKhoaHoc)
        .then((res) => {
          console.log(res.data);
          cloneThongTinKhoaHoc = res.data;
        })
        .catch((err) => {
          console.log(err);
        })
      return state.ThongTinKhoaHoc = { ...state, ThongTinKhoaHoc: cloneThongTinKhoaHoc }
    }
    default:
      return state;
  }
};
