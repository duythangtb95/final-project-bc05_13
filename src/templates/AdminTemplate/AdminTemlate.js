import React from 'react'
import { FileOutlined, PieChartOutlined, UserOutlined, DesktopOutlined, TeamOutlined } from '@ant-design/icons';
import { Breadcrumb, Layout, Menu, theme } from 'antd';
import { useState } from 'react';
import UserNav from '../../Components/Header/UserNav';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';


export default function AdminTemlate({ Component }) {

    let navigate = useNavigate();

    let user = useSelector((state) => {
        return state.userReducer.userInfo;
    });

    if (user === null) {
        window.location.assign("/*")
    }
    if (user.maLoaiNguoiDung === "HV") {
        window.location.assign("/*")
    }

    const { Header, Content, Footer, Sider } = Layout;
    function getItem(label, key, icon, children) {
        return {
            key,
            icon,
            children,
            label,
        };
    }
    const items = [
        getItem('Quản lý người dùng', '/admin/quanlynguoidung', <UserOutlined />),
        getItem('Quản lý khoá học', '/admin/quanlykhoahoc', <PieChartOutlined />),
    ];

    const [collapsed, setCollapsed] = useState(false);
    const {
        token: { colorBgContainer },
    } = theme.useToken();

    return (
        <Layout
            style={{
                minHeight: '100vh',
            }}
        >
            <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
                <div
                    className='hover:bg-sky-200'
                    style={{
                        height: 32,
                        margin: 16,
                        background: 'url("https://cybersoft.edu.vn/wp-content/uploads/2022/10/cyberlogo-white.png")',
                        backgroundSize: 'contain',
                        backgroundRepeat: 'no-repeat',
                        backgroundPosition: 'center',

                    }}
                    onClick={() => { navigate("/") }}
                />
                <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} onClick={(value) => { navigate(value.key) }} />
            </Sider>
            <Layout className="site-layout">

                <Header
                    style={{
                        padding: 0,
                        background: colorBgContainer,
                    }}
                >
                    <div className='flex flex-row-reverse'>
                        <UserNav />
                    </div>

                </Header>

                <Content
                    style={{
                        margin: '0 16px',
                    }}
                >
                    <Breadcrumb
                        style={{
                            margin: '16px 0',
                        }}
                    >

                    </Breadcrumb>
                    <div
                        style={{
                            padding: 24,
                            minHeight: 360,
                            background: colorBgContainer,
                        }}
                    >
                        <Component />

                    </div>
                </Content>

                <Footer
                    style={{
                        textAlign: 'center',
                    }}
                >
                    Team Design ©2023 Created by Team 13 - BC 05
                </Footer>
            </Layout>
        </Layout>


    )
}


