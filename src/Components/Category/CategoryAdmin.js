import React from "react";
import { FaChartPie, FaUser } from "react-icons/fa";
import { Dropdown, Space } from "antd";
import { Navigate, NavLink } from "react-router-dom";
import { Button } from 'antd';
import { useSelector } from "react-redux";
import { userLocalService } from "../../Services/localStoreServices/localStoreServices";

export default function CategoryAdmin() {
    let user = useSelector((state) => {
        return state.userReducer.userInfo;
    });
    let hanldLogOut = () => {
        userLocalService.remove();
        window.location.href = "/login";
    };

    let displayQL = (user.maLoaiNguoiDung === 'GV') ? "" : "hidden";
    const items = [
        {
            key: '1',
            label: (
                <NavLink onClick={() => { window.location.href = "/admin/quanlynguoidung" }} className={displayQL}>
                    Quản lý người dùng
                </NavLink>
            ),

        },
        {
            key: '2',
            label: (
                <NavLink onClick={() => { window.location.href = "/admin/quanlykhoahoc" }} className={displayQL}>
                    Quản lý khoá học
                </NavLink>
            ),

        },
        {
            key: '3',
            label: (
                <Button type="primary" onClick={hanldLogOut} danger>
                    Đăng Xuất
                </Button>

            ),
        }
    ]

    return (
        <div>
            <Dropdown
                className="hover:cursor-pointer hover:text-red-500 px-4 py-2 border-1 border-ray"
                trigger={["click"]}
                menu={{ items }}
            >
                <a onClick={(e) => e.preventDefault()}>
                    <Space className=" text-xl ">
                        <FaUser />
                        Chào {user.hoTen}
                    </Space>
                </a>
            </Dropdown>
        </div>
    );
}