import React from "react";

export default function Carousel() {
  return (
    <div
      className="bg-cover"
      style={{
        width: "100%",
        height: "500px",
        backgroundPosition: "50% 50%",
        backgroundImage:
          "url(https://thumbs.dreamstime.com/b/web-coding-software-development-concept-banner-laptop-program-code-screen-download-cloud-service-remote-work-245636154.jpg)",
      }}
    >
      <div className="flex justify-start ml-14 items-center w-full h-full">
        <div className="text-yellow-600 w-1/3 font-bold">
          <h1 className="text-5xl w-1/2">KHỞI ĐẦU SỰ NGHIỆP CỦA BẠN</h1>
          <h3 className="text-gray-200 text-2xl py-6">
            Trở thành lập trình viên chuyên nghiệp tại CyberSoft
          </h3>
          <div>
            <button className="bg-yellow-600 border-yellow-600 border-2 text-gray-200 px-4 py-2 ">
              Xem Khoá Học
            </button>
            <button className="ml-6 px-4 py-2  text-gray-200 border-2 border-gray-200 hover:text-gray-200 hover:bg-yellow-600 hover:border-yellow-600">
              Tư Vấn Học
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
