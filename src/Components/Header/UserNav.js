import React from "react";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import { userLocalService } from "../../Services/localStoreServices/localStoreServices";
import CategoryAdmin from "../Category/CategoryAdmin";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  let hanldLogOut = () => {
    userLocalService.remove();
    window.location.href = "/login";
  };

  console.log("thang: ",user);

  const renderContent = () => {
    if (user) {
      return (
        <>
          <CategoryAdmin hanldLogOut={hanldLogOut} />
        </>
      );
    } else {
      return (
        <>

          <NavLink to={"/login"}>
            <button className="rounded text-gray-400 text-lg hover:text-red-500 px-4 py-2">
              Đăng nhập
            </button>
          </NavLink>
          <NavLink to={"/signup"}>
            <button className="border-l-2 text-gray-400 text-lg hover:text-red-500 rounded px-4 py-2">
              Đăng Ký
            </button>
          </NavLink>
        </>
      );
    }
  };
  return <span className="space-x-5">{renderContent()}</span>;
}
