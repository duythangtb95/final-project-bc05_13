import React from "react";
import { Input, Space } from "antd";
import { useNavigate } from "react-router-dom";
const { Search } = Input;

export default function SearchKhoaHoc() {
  const navigate = useNavigate();
  const onSearch = (value) => {
    console.log(value);
    navigate("/search");
  };
  return (
    <div>
      <Space direction="vertical">
        <Search
          placeholder="Nhập tên khoá học"
          onSearch={onSearch}
          size="large"
          allowClear
        />
      </Space>
    </div>
  );
}
