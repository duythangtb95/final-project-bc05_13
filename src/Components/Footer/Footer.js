import React from "react";
import { FaMap } from "react-icons/fa";
import { FaPhone } from "react-icons/fa";

import logo from "../Header/logo/logo.png";
import img from "./img/img-footer.png";

export default function Footer() {
  return (
    <div className="px-6 py-10 bg-gray-800 text-gray-200">
      <div className="flex justify-between gap-5">
        <div className="w-1/3">
          <img className="w-56 object-cover" src={logo} alt="" />
          <p>
            CyberSoft Academy - Hệ thống đào tạo lập trình chuyên sâu theo dự án
            thực tế.
          </p>

          <h2 className="mt-10 font-bold">NHẬN TIN SỰ KIỆN & KHUYẾN MÃI</h2>
          <p>
            Cybersoft sẽ gởi các khoá học trực tuyến & các chương trình
            Cyberlive hoàn toàn MIỄN PHÍ và các chương trình KHUYẾN MÃI hấp dẫn
            đến các bạn.
          </p>
          <div>
            <button className="rounded px-4 py-2  bg-gray-200 text-gray-500  border-2 border-gray-200">
              your.address@email.com
            </button>
            <button className="rounded ml-8 bg-yellow-600 border-yellow-600 border-2 text-gray-200 px-4 py-2">
              ĐĂNG KÝ
            </button>
          </div>
        </div>
        <div className="w-1/3">
          <h2 className="mb-6 font-bold">ĐĂNG KÝ TƯ VẤN</h2>
          <input
            className="w-full mb-4 py-1 block"
            type="text"
            placeholder="Họ và tên *"
          />
          <input
            className="w-full mb-4 py-1 block"
            type="text"
            placeholder="Email liên hệ *"
          />
          <input
            className="w-full mb-4 py-1 block"
            type="text"
            placeholder="Điện thoại liên hệ *"
          />
          <h3>Nhấn vào ô bên dưới</h3>
          <div className="flex justify-between items-center bg-gray-200 py-4 text-gray-500 px-4">
            <div className="flex items-center">
              <input className="w-5 h-5 mr-2" type="checkbox" />
              <p>I'm not a robot</p>
            </div>
            <div>ReCapcha</div>
          </div>
          <button className="mt-2 bg-yellow-600 border-yellow-600 border-2 text-gray-200 px-4 py-2">
            Đăng Ký Tư Vấn
          </button>
        </div>
        <div className="w-1/3">
          <img src={img} className="object-cover" alt="" />
        </div>
      </div>
      <div className="flex justify-between gap-5 my-4">
        <div className="w-1/3">
          <div>
            <FaMap className="inline mr-2" />
            <span>Cơ sở 1: 376 Võ Văn Tần - Quận 3</span>
          </div>
          <div>
            <FaMap className="inline mr-2" />
            <span>Cơ sở 2: 459 Sư Vạn Hạnh - Quận 10</span>
          </div>
          <div>
            <FaMap className="inline mr-2" />
            <span>Cơ sở 3: 82 Ung Văn Khiêm - Bình Thạch</span>
          </div>
          <div>
            <FaMap className="inline mr-2" />
            <span>Cơ sở 4: Đà Nẵng - Quận Hải Châu</span>
          </div>
          <div>
            <FaPhone className="inline mr-2" />
            <span>096.105.1014 - 098.407.5835</span>
          </div>
        </div>
        <div className="w-1/3">
          <p>
            Lập trình Front End Lập trình React JS Lập trình React Lập trình tu
            duy Lập trình NodeS Lập trình aboviard Lập trình lava Wot Lập trình
            Java Spring - Jave Boot Tôi Đi Code Dạo Học SEO Hà Nội & Vietmoz Học
            Lập trình trực tuyến
          </p>
        </div>
        <div className="w-1/3">
          Anh ngữ giao tiếp - Khởi động anh ngữ giao tiếp - Lấy đà anh ngữ giao
          tiếp - Bật nhảy anh ngữ giao tiếp - Bay trên không anh ngữ giao tiếp -
          Tiếp đất
        </div>
      </div>
    </div>
  );
}
