import React, { useEffect, useState } from "react";
import Carousel from "../../Components/Carousel/Carousel";
import CarouselItem from "../../Components/CarouselItems/CarouselItem";
import { elearningService } from "../../Services/elearningService";

export default function Index() {
  const [dataElearning, setDataElearning] = useState([]);

  useEffect(() => {
    elearningService
      .getDanhSachKhoaHoc()
      .then((res) => {
        setDataElearning(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <Carousel />
      <div id="DanhSachKhoaHoc" className="container mx-auto">
        <h1 className="text-2xl text-center mt-5 mb-5 font-bold">
          CÁC KHOÁ HỌC MỚI NHẤT
        </h1>
        <CarouselItem dataElearning={dataElearning} />
      </div>
    </div>
  );
}
