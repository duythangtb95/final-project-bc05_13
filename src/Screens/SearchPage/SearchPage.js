import React, { useEffect, useState } from "react";
import { elearningService } from "../../Services/elearningService";

export default function SearchPage() {
  const [dataElearning, setDataElearning] = useState([]);

  useEffect(() => {
    elearningService
      .getDanhSachKhoaHoc()
      .then((res) => {
        setDataElearning(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return <div>{console.log("Search Data", dataElearning)}SearchPage</div>;
}
