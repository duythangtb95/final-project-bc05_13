import React from "react";
import "./NotPountPage.css";

export default function NotPountPage() {
  return (
    <div className="flex justify-center items-center">
      <div className="mt-5">
        <div className="number">404</div>
        <div className="text">
          <span>Ooops...</span>
          <br />
          page not found
        </div>
        <a
          className="me"
          href="https://codepen.io/uzcho_/pens/popular/?grid_type=list"
          target="_blank"
        />
      </div>
    </div>
  );
}
