import React, { useState } from 'react';
import { Button, Modal, Form, Input, message, Select } from 'antd';
import { adminServices } from '../../../Services/AdminService/adminService';

export default function PopUpAddUser() {

    const [isModalOpen, setIsModalOpen] = useState(false);

    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    // content popud
    //meseger thông báo người dùng
    const [messageApi, contextHolder] = message.useMessage();

    const error = (value) => {
        messageApi.open({
            type: 'error',
            content: value,
        });
    };

    const success = (value) => {
        messageApi.open({
            type: 'success',
            content: value,
        });
    };


    const onFinish = (values) => {
        adminServices
            .postThemNguoiDung({...values, "maNhom": "GP01" })
            .then((res) => {
                success("đã thêm thành công");
                setTimeout(window.location.reload(),5000)
            })
            .catch((err) => {
                console.log(err);;
            })
        console.log('Success:', Object.assign(values, { "maNhom": "GP01" }));

        console.log(values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div>
            <>
                <Button type="primary" onClick={showModal} danger>
                    Thêm người dùng
                </Button>
                <Modal title="Thêm người dùng" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                    <div>
                        {contextHolder}
                        <div>
                            <Form
                                name="basic"
                                labelCol={{
                                    span: 8,
                                }}
                                wrapperCol={{
                                    span: 16,
                                }}
                                style={{
                                    maxWidth: 600,
                                }}
                                initialValues={{
                                    remember: true,
                                }}
                                onFinish={onFinish}
                                onFinishFailed={onFinishFailed}
                                autoComplete="off"
                            >
                                <Form.Item
                                    label="Tài khoản"
                                    name="taiKhoan"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Hãy nhập mật khẩu!',
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label="Mật khẩu"
                                    name="matKhau"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Hãy nhập mật khẩu!',
                                        },
                                    ]}
                                >
                                    <Input.Password />
                                </Form.Item>

                                <Form.Item
                                    label="Họ Tên"
                                    name="hoTen"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Hãy nhập Họ Tên!',
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label="Số điện thoại"
                                    name="soDT"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Hãy nhập số điện thoại!',
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label="Email"
                                    name="email"
                                    rules={[
                                        {
                                            type: 'email',
                                            required: true,
                                            message: 'Hãy nhập email!',

                                        },
                                    ]}
                                >
                                    <Input
                                    />
                                </Form.Item>

                                <Form.Item
                                    label="Loại người dùng"
                                    name='maLoaiNguoiDung'
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Hãy nhập chọn loại tài khoản!',

                                        },
                                    ]}
                                >
                                    <Select>
                                        <Select.Option value="GV">Giáo vụ</Select.Option>
                                        <Select.Option value="HV">Học Viên</Select.Option>
                                    </Select>
                                </Form.Item>

                                <Form.Item
                                    wrapperCol={{
                                        offset: 8,
                                        span: 16,
                                    }}
                                >
                                    <Button type="primary" htmlType="submit" danger
                                    >
                                        Cập nhật
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                </Modal>
            </>
        </div>
    )
}
