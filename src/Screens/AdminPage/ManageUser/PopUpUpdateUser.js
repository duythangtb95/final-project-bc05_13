import React, { useState } from 'react';
import { Button, Modal, Form, Input, message, Select } from 'antd';
import { adminServices } from '../../../Services/AdminService/adminService';

export default function PopUpUpdateUser({ infoUser }) {

    const [isModalOpen, setIsModalOpen] = useState(false);

    const showModal = () => {
        setIsModalOpen(true);
        console.log(infoUser)
    };

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    let { email, hoTen, maLoaiNguoiDung, soDt, taiKhoan } = infoUser
    // content popud
    //meseger thông báo người dùng
    const [messageApi, contextHolder] = message.useMessage();

    const error = (value) => {
        messageApi.open({
            type: 'error',
            content: value,
        });
    };

    const success = () => {
        messageApi.open({
            type: 'success',
            content: 'Cập nhật thành công',
        });
    };

    // const [InfoAddUser, setInfoAddUser] = useState();


    const onFinish = (values) => {



        adminServices
            .putCapNhatThongTinNguoiDung({...values,taiKhoan,maLoaiNguoiDung, "maNhom": "GP01" },)
            .then((res) => {
                success();
                setTimeout(window.location.reload(),5000)
                console.log("đã thêm", res);
            })
            .catch((err) => {
                error(err.response.data);
                console.log(err);

            })

        console.log('Success:', {...values,taiKhoan});
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div>
            <>
                <Button type="primary" onClick={showModal} danger>
                    Sửa
                </Button>
                <Modal title="Sửa" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>

                    <div>
                        {contextHolder}
                        <div>
                            <Form
                                name="basic"
                                labelCol={{
                                    span: 8,
                                }}
                                wrapperCol={{
                                    span: 16,
                                }}
                                style={{
                                    maxWidth: 600,
                                }}
                                initialValues={{
                                    remember: true,
                                }}
                                onFinish={onFinish}
                                onFinishFailed={onFinishFailed}
                                autoComplete="off"
                            >
                                <Form.Item
                                    label="Tài Khoản"
                                    rules={[
                                        {
                                            required: false,
                                        },
                                    ]}
                                    valuePropName={taiKhoan}
                                >
                                    {taiKhoan}
                                </Form.Item>

                                <Form.Item
                                    label="Mật khẩu"
                                    name="matKhau"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Hãy nhập mật khẩu!',
                                        },
                                    ]}
                                >
                                    <Input.Password />
                                </Form.Item>

                                <Form.Item
                                    label="Họ Tên"
                                    name="hoTen"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Nhập Họ Tên!',
                                        },
                                    ]}
                                    initialValue={hoTen}
                                >
                                    <Input
                                        defaultValue={hoTen}
                                    />
                                </Form.Item>

                                <Form.Item
                                    label="Số điện thoại"
                                    name="soDT"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Nhập số điện thoại!',
                                        },
                                    ]}
                                    initialValue={soDt}
                                >
                                    <Input
                                        defaultValue={soDt}
                                    />
                                </Form.Item>
                                <Form.Item
                                    label="Email"
                                    name="email"
                                    rules={[
                                        {
                                            type: 'email',
                                            required: true,
                                            message: 'Nhập Email!',
                                        },
                                    ]}
                                    initialValue={email}
                                >
                                    <Input
                                        defaultValue={email}
                                    />
                                </Form.Item>
                                <Form.Item
                                    label="Loại người dùng"
                                    name='maLoaiNguoiDung'
                                    initialValue={maLoaiNguoiDung}
                                >
                                    <Select>
                                        <Select.Option value="GV">Giáo vụ</Select.Option>
                                        <Select.Option value="HV">Học Viên</Select.Option>
                                    </Select>
                                </Form.Item>

                                <Form.Item
                                    wrapperCol={{
                                        offset: 8,
                                        span: 16,
                                    }}
                                >
                                    <Button type="primary" htmlType="submit"
                                    >
                                        Submit
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                </Modal>
            </>
        </div>
    )
}
