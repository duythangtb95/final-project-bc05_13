import React, { useEffect, useState } from 'react'
import { Space, Table, Tag, Button, Input, message } from 'antd';
import { adminServices } from '../../../Services/AdminService/adminService';
import PopUpAddUser from './PopUpAddUser';
import PopUpResgiterUser from './PopUpResgiterUser';
import PopUpUpdateUser from './PopUpUpdateUser';



export default function ManageUser() {


    const [UserList, setUserList] = useState([])

    // tìm kiếm 
    const { Search } = Input;

    const onSearch = (value) => {
        if (value === '') {
            adminServices
                .getDanhSachNguoiDung()
                .then((res) => {
                    let UserList1 = res.data.map((item, index) => {
                        return { ...item, sTT: (index + 1), }
                    });
                    setUserList(UserList1);
                })
                .catch((err) => {
                    console.log(err);
                });
        } else {
            adminServices
                .getTimKiemNguoiDung(value)
                .then((res) => {
                    let UserList1 = res.data.map((item, index) => {
                        return { ...item, sTT: (index + 1), }
                    });
                    setUserList(UserList1);
                })
                .catch((err) => {
                    console.log(err);
                })
        }
        console.log(value);
    };
    // end tìm kiếm

    useEffect(() => {
        adminServices
            .getDanhSachNguoiDung()
            .then((res) => {
                let UserList1 = res.data.map((item, index) => {
                    return { ...item, sTT: (index + 1), }
                });
                setUserList(UserList1);
                console.log(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [])

    const deleteUser = (value) => {
        adminServices
            .deleteXoaNguoiDung(value)
            .then((res) => {
                success(res.data)
                setTimeout(window.location.reload(), 5000)
            })
            .catch((err) => {
                error(err.response.data)
            })
        console.log(value)

    }

    const columns = [
        {
            title: 'STT',
            dataIndex: 'sTT',
            key: 'sTT',
        },
        {
            title: 'Tài khoản',
            dataIndex: 'taiKhoan',
            key: 'taiKhoan',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Họ tên',
            dataIndex: 'hoTen',
            key: 'hoTen',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Số điện thoại',
            key: 'soDt',
            dataIndex: 'soDt',
        },
        {
            title: 'Loại',
            key: 'maLoaiNguoiDung',
            dataIndex: 'maLoaiNguoiDung',
            render: (_, { maLoaiNguoiDung }) => {
                let color = 'green';
                if (maLoaiNguoiDung === 'GV') {
                    color = 'volcano';
                };
                return (
                    <Tag color={color} key={maLoaiNguoiDung}>
                        {maLoaiNguoiDung}
                    </Tag>
                )
            },
        },
        {
            title: 'Thao Tác',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <PopUpResgiterUser
                        taiKhoan={record.taiKhoan}
                        hoTen={record.hoTen} />
                    <PopUpUpdateUser
                        infoUser={record}
                        onClick={() => { console.log("thắng"); }} />
                    <Button type='primary' onClick={() => deleteUser(record.taiKhoan)} danger>Xoá</Button>
                </Space>
            ),
        },
    ];

    //meseger thông báo người dùng
    const [messageApi, contextHolder] = message.useMessage();

    const error = (value) => {
        messageApi.open({
            type: 'error',
            content: value,
        });
    };

    const success = (value) => {
        messageApi.open({
            type: 'success',
            content: value,
        });
    };

    return (

        <div>
            {contextHolder}
            <h1> Quản lý người dùng</h1>

            <PopUpAddUser />
            <div className='my-2'>
                <Search placeholder="Nhập họ tên người dùng" onSearch={onSearch} enterButton />
            </div>


            <Table columns={columns} dataSource={UserList} />
        </div>
    )
}
