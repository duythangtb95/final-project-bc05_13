import React, { useState } from 'react';
import { Button, Modal, Space, Table, AutoComplete, message } from 'antd';
import { adminServices } from '../../../Services/AdminService/adminService';

export default function PopUpResgiterUser({ taiKhoan,hoTen }) {

    const [DanhSachKhoaHocChuaGhiDanh, setDanhSachKhoaHocChuaGhiDanh] = useState([]);
    const [DanhSachKhoaHocDaXetDuyet, setDanhSachKhoaHocDaXetDuyet] = useState([])
    const [DanhSachKhoaHocChoXetDuyet, setDanhSachKhoaHocChoXetDuyet] = useState([])
    const [KhoaHocGhiDanh, setKhoaHocGhiDanh] = useState([])


    const [isModalOpen, setIsModalOpen] = useState(false);

    // function load danh sách khoá học chưa ghi danh
    const LoadDanhSachKhoaHocChuaGhiDanh = (taiKhoan) => { 
        adminServices
        .RUpostLayDanhSachKhoaHocChuaGhiDanh(taiKhoan)
        .then((res) => {
            let listCouse = (res.data.map((item) => { return { "value": item.tenKhoaHoc, "maKhoaHoc": item.maKhoaHoc } }))
            setDanhSachKhoaHocChuaGhiDanh(listCouse)
            console.log(listCouse);
        })
        .catch((err) => {
            console.log(err);
        })
     }

    // function load danh sách khoá học đã xét duyệt
    const LoadDanhSachKhoaHocDaXetDuyet = (taiKhoan) => {
        adminServices
            .RUpostLayDanhSachKhoaHocDaXetDuyet({ taiKhoan })
            .then((res) => {
                let listCourseDaXetDuyet = (res.data.map((item, index) => {
                    return {
                        sTT: index + 1,
                        tenKhoaHoc: item.tenKhoaHoc,
                        maKhoaHoc: item.maKhoaHoc,
                    }
                }))
                setDanhSachKhoaHocDaXetDuyet(listCourseDaXetDuyet)
                console.log("danh sách khoá học xét duyệ", res.data);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    // function load danh sách khoá học chờ xét duyệt
    const LoadDanhSachKhoaHocChoXetDuyet = (taiKhoan) => {
        adminServices
            .RUpostLayDanhSachKhoaHocChoXetDuyet({ taiKhoan })
            .then((res) => {
                let listCourseChoXetDuyet = (res.data.map((item, index) => {
                    return {
                        sTT: index + 1,
                        tenKhoaHoc: item.tenKhoaHoc,
                        maKhoaHoc: item.maKhoaHoc,
                    }
                }))
                setDanhSachKhoaHocChoXetDuyet(listCourseChoXetDuyet)
                console.log("danh sách khoá học xét duyệ", res.data);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    const showModal = () => {
        setIsModalOpen(true);
        // load danh sách khoá học chưa ghi danh
        LoadDanhSachKhoaHocChuaGhiDanh(taiKhoan)
        // load danh sách khoá học đã xét duyệt
        LoadDanhSachKhoaHocDaXetDuyet(taiKhoan)
        // load danh sách khoá học chờ xét duyệt
        LoadDanhSachKhoaHocChoXetDuyet(taiKhoan)

    };

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };


    // content popup


    // const { Search } = Input;

    const ghiDanh = (taiKhoan, maKhoaHoc) => {
        adminServices
            .RUpostGhiDanhKhoaHoc(taiKhoan, maKhoaHoc)
            .then((res) => {
                LoadDanhSachKhoaHocChuaGhiDanh(taiKhoan)
                LoadDanhSachKhoaHocDaXetDuyet(taiKhoan)
                LoadDanhSachKhoaHocChoXetDuyet(taiKhoan)
                success(res.data)
                console.log(res);
            })
            .catch((err) => {
                error(err.response.data)
                console.log(err);
            })
    }

    const HuyKhoaHoc = (taiKhoan, maKhoaHoc) => {
        adminServices
            .RUpostHuyGhiDanh(taiKhoan, maKhoaHoc)
            .then((res) => {
                success(res.data)
                LoadDanhSachKhoaHocDaXetDuyet(taiKhoan)
                LoadDanhSachKhoaHocChoXetDuyet(taiKhoan)
                console.log(res);
            })
            .catch((err) => {
                error(err.response.data)
                console.log(err);
            })
    }
    const KhoaHocCho = [
        {
            title: 'STT',
            dataIndex: 'sTT',
            key: 'sTT',
        },
        {
            title: 'Tên khoá học',
            dataIndex: 'tenKhoaHoc',
            key: 'tenKhoaHoc',
            render: (text) => <a>{text}</a>,

        },
        {
            title: 'Chờ xác nhận',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <Button type="primary" onClick={() => { ghiDanh(taiKhoan, record.maKhoaHoc) }} danger>
                        Xác thực
                    </Button>
                    <Button type="primary" onClick={() => { HuyKhoaHoc(taiKhoan, record.maKhoaHoc); }} danger>
                        Huỷ
                    </Button>
                </Space>
            ),
        },
    ];

    const KhoaHocXacNhan = [
        {
            title: 'STT',
            dataIndex: 'sTT',
            key: 'sTT',
        },
        {
            title: 'Tên khoá học',
            dataIndex: 'tenKhoaHoc',
            key: 'tenKhoaHoc',
            render: (text) => <a>{text}</a>,

        },
        {
            title: 'Chờ xác nhận',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <Button type="primary" onClick={() => { HuyKhoaHoc(taiKhoan, record.maKhoaHoc) }} danger>
                        Huỷ
                    </Button>
                </Space>
            ),
        },
    ];


    //meseger thông báo người dùng
    const [messageApi, contextHolder] = message.useMessage();

    const error = (value) => {
        messageApi.open({
            type: 'error',
            content: value,
        });
    };

    const success = (value) => {
        messageApi.open({
            type: 'success',
            content: value,
        });
    };

    return (
        <div>
            <>
                <Button type="primary" onClick={showModal} danger>
                    Ghi Danh
                </Button>
                <Modal title={hoTen} open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                    <div>
                        <div className='my-5'>
                            {contextHolder}
                            <AutoComplete
                                style={{ width: '70%' }}
                                placeholder="Chọn tên khoá học"
                                onSearch={(value, record) => { setKhoaHocGhiDanh(record.maKhoaHoc) }}
                                onChange={(value, record) => { setKhoaHocGhiDanh(record.maKhoaHoc) }}
                                options={DanhSachKhoaHocChuaGhiDanh}
                            />
                            <Button
                                type="primary"
                                className='mx-1'
                                onClick={() => { ghiDanh(taiKhoan, KhoaHocGhiDanh) }}
                                danger
                            >
                                Ghi Danh
                            </Button>
                        </div>
                        <div>
                            <Table columns={KhoaHocCho} dataSource={DanhSachKhoaHocChoXetDuyet} />
                        </div>
                    </div>
                    <div>
                        <div>
                            <Table columns={KhoaHocXacNhan} dataSource={DanhSachKhoaHocDaXetDuyet} />
                        </div>
                    </div>
                </Modal>
            </>
        </div>
    )
}
