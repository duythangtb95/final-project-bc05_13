import React, { useEffect, useState } from 'react'
import { Space, Table, Tag, Button, Input, message } from 'antd';
import { adminServices } from '../../../Services/AdminService/adminService';
import PopUpAddCourse from './PopUpAddCourse';
import PopUpResgiterCourse from './PopUpResgiterCourse';
import PopUpUpdateCourse from './PopUpUpdateCourse';

export default function ManageCourse() {

  const [CourseList, setCourseList] = useState([])
  const [DanhMucKhoaHoc, setDanhMucKhoaHoc] = useState([]);

  // tìm kiếm 
  const { Search } = Input;

  const onSearch = (value) => {
    if (value === '') {
      adminServices
        .getLayDanhSachKhoaHoc()
        .then((res) => {
          let CourseList1 = res.data.map((item, index) => {
            return { ...item, sTT: (index + 1) }
          });
          setCourseList(CourseList1);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      adminServices
        .getLayDanhMucKhoaHocValue(value)
        .then((res) => {
          let CourseList1 = res.data.map((item, index) => {
            return { ...item, sTT: (index + 1) }
          });
          setCourseList(CourseList1);
        })
        .catch((err) => {
          console.log(err);
        });
    }
    console.log(value);
  };
  // end tìm kiếm

  useEffect(() => {

    // lấy danh sách khoá học
    adminServices
      .getLayDanhSachKhoaHoc()
      .then((res) => {
        let CourseList1 = res.data.map((item, index) => {
          return { ...item, sTT: index + 1 }
        });
        setCourseList(CourseList1);
      })
      .catch((err) => {
        console.log(err);
      });

    // lấy danh mục khoá học
    adminServices
      .getLayDanhMucKhoaHoc()
      .then((res) => {
        setDanhMucKhoaHoc(res.data);
      })
      .catch((err) => {
        console.log(err);
      })
  }, [])

  const deleteCourse = (value) => {
    adminServices
      .deleteXoaKhoaHoc(value)
      .then((res) => {
        success(res.data)
        console.log(res);
        setTimeout(() => {
          window.location.reload()
        }, 1000)
      })
      .catch((err) => {
        error(err.response.data)
        console.log(err);
      })
  }

  const columns = [
    {
      title: 'STT',
      dataIndex: 'sTT',
      key: 'sTT',
    },
    {
      title: 'Mã Khoá Học',
      dataIndex: 'maKhoaHoc',
      key: 'maKhoaHoc',
      render: (text) => <a key={text}>{text}</a>,
    },
    {
      title: 'Tên khoá học',
      dataIndex: 'tenKhoaHoc',
      key: 'tenKhoaHoc',
    },
    {
      title: 'Hình Ảnh',
      dataIndex: 'hinhAnh',
      key: 'hinhAnh',
      render: (hinhAnh) => <img width={200} alt={hinhAnh} src={hinhAnh} />
    },
    {
      title: 'Lượt xem',
      key: 'luotXem',
      dataIndex: 'luotXem',
    },
    {
      title: 'Người tạo',
      key: 'nguoiTao',
      dataIndex: 'nguoiTao',
      render: (nguoiTao) => <p key={nguoiTao}>{nguoiTao.hoTen}</p>,
    },
    {
      title: 'Danh mục khoá học',
      key: 'danhMucKhoaHoc',
      dataIndex: 'danhMucKhoaHoc',
      render: (_, { danhMucKhoaHoc }) => {
        let color = 'green';
        let maDanhMucKhoahoc = danhMucKhoaHoc.maDanhMucKhoahoc
        let tenDanhMucKhoaHoc = danhMucKhoaHoc.tenDanhMucKhoaHoc
        if (maDanhMucKhoahoc === 'FrontEnd') {
          color = 'magenta';
        };
        if (maDanhMucKhoahoc === 'Design') {
          color = 'red';
        };
        if (maDanhMucKhoahoc === 'DiDong') {
          color = 'cyan';
        };
        if (maDanhMucKhoahoc === 'FullStack') {
          color = 'blue';
        };
        if (maDanhMucKhoahoc === 'BackEnd') {
          color = 'purple';
        };
        return (
          <Tag color={color} key={maDanhMucKhoahoc}>
            {tenDanhMucKhoaHoc}
          </Tag>
        )
      },
    },
    {
      title: 'Thao Tác',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <PopUpResgiterCourse
            maKhoaHoc={record.maKhoaHoc}
            tenKhoaHoc={record.tenKhoaHoc}
          />
          <PopUpUpdateCourse
            DanhMucKhoaHoc={DanhMucKhoaHoc}
            infoKhoaHoc={record}
          />
          <Button type='primary' onClick={() => deleteCourse(record.maKhoaHoc)} danger>Xoá</Button>
        </Space>
      ),
    },
  ];;

  //meseger thông báo người dùng
  const [messageApi, contextHolder] = message.useMessage();

  const error = (value) => {
    messageApi.open({
      type: 'error',
      content: value,
    });
  };

  const success = (value) => {
    messageApi.open({
      type: 'success',
      content: value,
    });
  };

  return (
    <div>
      {contextHolder}
      <PopUpAddCourse DanhMucKhoaHoc={DanhMucKhoaHoc} />
      <div className='my-2'>
        <Search placeholder="Nhập tên khoá học" onSearch={onSearch} enterButton />
      </div>
      <Table columns={columns} dataSource={CourseList} />
    </div>)
}
