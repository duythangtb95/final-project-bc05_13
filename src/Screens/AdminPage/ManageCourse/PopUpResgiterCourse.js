import React, { useState } from 'react';
import { Button, Modal, Space, Table, AutoComplete, message } from 'antd';
import { adminServices } from '../../../Services/AdminService/adminService';


export default function PopUpResgiterCourse({ maKhoaHoc, tenKhoaHoc }) {

    const [DanhSachHocVienChuaGhiDanh, setDanhSachHocVienChuaGhiDanh] = useState([]);
    const [DanhSachHocVienKhoaHoc, setDanhSachHocVienKhoaHoc] = useState([])
    const [DanhSachKhoaHocChoXetDuyet, setDanhSachKhoaHocChoXetDuyet] = useState([])
    const [HocVienGhiDanh, setHocVienGhiDanh] = useState([])

    const [isModalOpen, setIsModalOpen] = useState(false);

    // function load danh sách người dùng chưa ghi danh
    const LoadDanhSachHocVienChuaGhiDanh = () => {
        adminServices
            .RCpostLayDanhSachNguoiDungChuaGhiDanh({ maKhoaHoc })
            .then((res) => {
                let listUserData = (res.data.map((item) => { return { "value": item.hoTen, "taiKhoan": item.taiKhoan } }))
                setDanhSachHocVienChuaGhiDanh(listUserData)
                console.log("danh người dùng chưa ghi danh", res.data);
            })
            .catch((err) => {
                console.log(err);
            })
    }
    // function load danh sách người dùng đã ghi danh vào khóa học 
    const LoadDanhSachHocVienKhoaHoc = () => {
        adminServices
            .RCpostLayDanhSachHocVienKhoaHoc({ maKhoaHoc })
            .then((res) => {
                let listUserData = (res.data.map((item, index) => {
                    return {
                        sTT: index + 1,
                        hoTen: item.hoTen,
                        taiKhoan: item.taiKhoan,
                    }
                }))
                setDanhSachHocVienKhoaHoc(listUserData)
                console.log("danh người dùng chưa ghi danh", res.data);
            })
            .catch((err) => {
                console.log(err);
            })
    }
    // functino load danh sách học viên chờ xét duyệt khoá học
    const LoadDanhSachHocVienChoXetDuyet = () => {
        adminServices
            .RCpostLayDanhSachHocVienChoXetDuyet({ maKhoaHoc })
            .then((res) => {
                let listUserData = (res.data.map((item, index) => {
                    return {
                        sTT: index + 1,
                        hoTen: item.hoTen,
                        taiKhoan: item.taiKhoan,
                    }
                }))
                setDanhSachKhoaHocChoXetDuyet(listUserData)
                console.log("danh người dùng chưa ghi danh", res.data);
            })
            .catch((err) => {
                console.log(err);
            })
    }


    const showModal = () => {
        setIsModalOpen(true);
        LoadDanhSachHocVienChuaGhiDanh()
        LoadDanhSachHocVienChoXetDuyet()
        LoadDanhSachHocVienKhoaHoc()
    };

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };


    // content popup
    const [KhoaHocDuocChon, setKhoaHocDuocChon] = useState()

    const onSearch = (value) => {
        setKhoaHocDuocChon(value)
        console.log(value)
    };
    // const { Search } = Input;

    const ghiDanh = (taiKhoan, maKhoaHoc) => {
        adminServices
            .RCpostGhiDanhKhoaHoc(taiKhoan, maKhoaHoc)
            .then((res) => {
                success(res.data)
                LoadDanhSachHocVienChoXetDuyet()
                LoadDanhSachHocVienChuaGhiDanh()
                LoadDanhSachHocVienKhoaHoc()
            })
            .catch((err) => {
                error(err.response.data)
            })
    }

    const HuyGhiDanh = (taiKhoan, maKhoaHoc) => {
        adminServices
            .RCpostHuyGhiDanh(taiKhoan, maKhoaHoc)
            .then((res) => {
                success(res.data)
                LoadDanhSachHocVienChoXetDuyet()
                LoadDanhSachHocVienChuaGhiDanh()
                LoadDanhSachHocVienKhoaHoc()
            })
            .catch((err) => {
                error(err.response.data)
            })
    }

    const NguoiChuaXacNhan = [
        {
            title: 'STT',
            dataIndex: 'sTT',
            key: 'sTT',
        },
        {
            title: 'Tài Khoản',
            dataIndex: 'taiKhoan',
            key: 'taiKhoan',
        },
        {
            title: 'Họ tên',
            dataIndex: 'hoTen',
            key: 'hoTen',
        },
        {
            title: 'Chờ xác nhận',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <Button type="primary" onClick={() => { ghiDanh(record.taiKhoan, maKhoaHoc) }} danger>
                        Xác nhận
                    </Button>
                    <Button type="primary" onClick={() => { HuyGhiDanh(record.taiKhoan, maKhoaHoc) }} danger>
                        Huỷ
                    </Button>
                </Space>
            ),
        },
    ];

    const NguoiDungDaXacNhan = [
        {
            title: 'STT',
            dataIndex: 'sTT',
            key: 'sTT',
        },
        {
            title: 'Tài Khoản',
            dataIndex: 'taiKhoan',
            key: 'taiKhoan',
        },
        {
            title: 'Họ tên',
            dataIndex: 'hoTen',
            key: 'hoTen',
        },
        {
            title: 'Chờ xác nhận',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <Button type="primary" onClick={() => { HuyGhiDanh(record.taiKhoan, maKhoaHoc) }} danger>
                        Huỷ
                    </Button>
                </Space>
            ),
        },
    ];



    //meseger thông báo người dùng
    const [messageApi, contextHolder] = message.useMessage();

    const error = (value) => {
        messageApi.open({
            type: 'error',
            content: value,
        });
    };

    const success = (value) => {
        messageApi.open({
            type: 'success',
            content: value,
        });
    };

    return (
        <div>
            {contextHolder}
            <>
                <Button type="primary" onClick={showModal} danger>
                    Ghi Danh
                </Button>
                <Modal title={tenKhoaHoc} open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                    <div>
                        <div className='my-5'>
                            <AutoComplete
                                style={{ width: '70%' }}
                                placeholder="Chọn tên người dùng"
                                onSearch={(value, record) => { setHocVienGhiDanh(record.taiKhoan) }}
                                onChange={(value, record) => { setHocVienGhiDanh(record.taiKhoan) }}
                                options={DanhSachHocVienChuaGhiDanh}
                            />

                            <Button
                                type="primary"
                                className='mx-1'
                                onClick={() => { ghiDanh(HocVienGhiDanh, maKhoaHoc); }}
                                danger
                            >
                                Ghi Danh
                            </Button>
                        </div>
                        <div>
                            <Table columns={NguoiChuaXacNhan} dataSource={DanhSachKhoaHocChoXetDuyet} />
                        </div>
                    </div>
                    <div>
                        <div>
                            <Table columns={NguoiDungDaXacNhan} dataSource={DanhSachHocVienKhoaHoc} />
                        </div>
                    </div>
                </Modal>
            </>
        </div>
    )
}
