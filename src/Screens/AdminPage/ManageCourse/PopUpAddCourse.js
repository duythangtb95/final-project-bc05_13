import React, { useEffect, useState } from 'react';
import { Button, Modal, Form, Input, message, Select, Space, Upload } from 'antd';
import { adminServices } from '../../../Services/AdminService/adminService';
import { toKebabCase } from '../../../Uitilies/adminUtilies';
import { UploadOutlined } from '@ant-design/icons';
import moment from 'moment/moment';
import { userLocalService } from '../../../Services/localStoreServices/localStoreServices';


export default function PopUpAddCourse({ DanhMucKhoaHoc }) {

    const [image, setImage] = useState(null);
    const [nguoiTao, setNguoiTao] = useState({})


    useEffect(() => {
        // lấy thông tin người khởi tạo
        setNguoiTao(userLocalService.get())

    }, [])

    const renderDanhMucKhoaHoc = () => {
        let DanhMuc = DanhMucKhoaHoc.map((item) => {
            return <Select.Option value={item.maDanhMuc}>{item.tenDanhMuc}</Select.Option>
        })
        return (
            <Select>
                {DanhMuc}
            </Select>
        )
    }

    const [isModalOpen, setIsModalOpen] = useState(false);

    const showModal = () => {
        setIsModalOpen(true);
        console.log(nguoiTao)
    };

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };


    const today = new Date().toLocaleDateString();

    const UploadImage = (tenKhoaHoc) => {
        let frm = new FormData();
        frm.append("file", image);
        frm.append("tenKhoaHoc", tenKhoaHoc);
        console.log("frm: ", frm.get("tenKhoaHoc"));
        adminServices
            .postUploadHinhAnhKhoaHoc(frm)
            .then((res) => {
                success(res.data);
            })
            .catch((err) => {
                error(err.response.data);
            });
    };

    const onFinish = (values) => {
        let biDanh = toKebabCase(values.tenKhoaHoc);
        let DataPost = {
            ...values,
            taiKhoanNguoiTao: nguoiTao.taiKhoan,
            maNhom: "GP01",
            biDanh,
            ngayTao: moment(today).format("DD/MM/YYYY"),
            hinhAnh: image.name,
        };
        adminServices
            .postThemKhoaHoc(DataPost)
            .then((res) => {
                UploadImage(biDanh)
                success('Đã thêm khoá học thành công');
                console.log("đã thêm", res);
                setTimeout(() => { 
                    window.location.reload()
                 },1000)
            })
            .catch((err) => {
                error(err.response.data);
                console.log(err);

            })
        console.log('Success:', DataPost);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    // text area mô tả
    const { TextArea } = Input;
    const onChange = (e) => {
        console.log('Change:', e.target.value);
    };

    // content popud
    //meseger thông báo người dùng
    const [messageApi, contextHolder] = message.useMessage();

    const error = (value) => {
        messageApi.open({
            type: 'error',
            content: value,
        });
    };

    const success = (value) => {
        messageApi.open({
            type: 'success',
            content: value,
        });
    };
    return (
        <div>
            <>
                <Button type="primary" onClick={showModal} danger>
                    Thêm khoá học
                </Button>
                <Modal title="Thêm người dùng" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                    <div>
                        {contextHolder}
                        <div>
                            <Form
                                name="basic"
                                labelCol={{
                                    span: 8,
                                }}
                                wrapperCol={{
                                    span: 16,
                                }}
                                style={{
                                    maxWidth: 600,
                                }}
                                initialValues={{
                                    remember: true,
                                }}
                                onFinish={onFinish}
                                onFinishFailed={onFinishFailed}
                                autoComplete="off"
                            >


                                <Form.Item
                                    label="Mã khoá học"
                                    name="maKhoaHoc"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Nhập mã khoá học!',
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label="Tên khoá học"
                                    name="tenKhoaHoc"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Nhập tên khoá học!',
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label="Lượt xem"
                                    name="luotXem"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Nhập Họ Tên!',
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label="Lượt đánh giá"
                                    name="danhGia"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Nhập đánh giá!',
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label="Danh mục khoá học"
                                    name='maDanhMucKhoaHoc'
                                >
                                    {renderDanhMucKhoaHoc()}
                                </Form.Item>

                                <Form.Item
                                    label="Mô tả khóc học"
                                    name="moTa"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Nhập mô tả khoá học!',
                                        },
                                    ]}
                                >
                                    <TextArea
                                        showCount
                                        maxLength={100}
                                        style={{
                                            height: 120,
                                            resize: 'none',
                                        }}
                                        onChange={onChange}
                                        placeholder="Nhập mô tả khoá học!"
                                    />
                                </Form.Item>

                                <Form.Item
                                    label="Hình ảnh"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Chọn file hình ảnh!',
                                        },
                                    ]}
                                >
                                    <Upload
                                        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                                        listType="picture"
                                        maxCount={1}
                                        beforeUpload={(file) => {
                                            // Access file content here and do something with it
                                            console.log("file", file)
                                            setImage(file);
                                            // Prevent upload
                                            return false;
                                        }}
                                    >
                                        <Button icon={<UploadOutlined />}>Upload</Button>
                                    </Upload>
                                </Form.Item>


                                <Form.Item
                                    wrapperCol={{
                                        offset: 8,
                                        span: 16,
                                    }}
                                >
                                    <Button type="primary" htmlType="submit"
                                    >
                                        Submit
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                </Modal>
            </>
        </div>
    )
}
