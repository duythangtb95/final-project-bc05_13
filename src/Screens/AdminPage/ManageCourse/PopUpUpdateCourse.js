import React, { useEffect, useState } from 'react';
import { Button, Modal, Form, Input, message, Select, Space, Upload } from 'antd';
import { adminServices } from '../../../Services/AdminService/adminService';
import { UploadOutlined } from '@ant-design/icons';
import { toKebabCase } from '../../../Uitilies/adminUtilies';

export default function PopUpUpdateCourse({ DanhMucKhoaHoc, infoKhoaHoc }) {

    const [image, setImage] = useState(null);


    // render danh mục
    const renderDanhMucKhoaHoc = () => {
        let DanhMuc = DanhMucKhoaHoc.map((item) => {
            return <Select.Option value={item.maDanhMuc}>{item.tenDanhMuc}</Select.Option>
        })
        return (
            <Select>
                {DanhMuc}
            </Select>
        )
    }
    // get lấy thông tin khoá học từ mã

    const [isModalOpen, setIsModalOpen] = useState(false);

    const showModal = () => {
        setIsModalOpen(true);
    };

    let { maKhoaHoc, danhMucKhoaHoc, tenKhoaHoc, luotXem, moTa, hinhAnh, ngayTao, nguoiTao, maNhom
    } = infoKhoaHoc;

    const [fileList, setFileList] = useState([
        {
            uid: '-1',
            name: `${hinhAnh}`,
            status: 'done',
            url: `${hinhAnh}`,
        },
    ])

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };


    const today = new Date().toLocaleDateString();

    const UploadImage = (tenKhoaHoc) => {
        let frm = new FormData();
        frm.append("file", image);
        frm.append("tenKhoaHoc", tenKhoaHoc);
        adminServices
            .postUploadHinhAnhKhoaHoc(frm)
            .then((res) => {
                success("Upload hình ảnh thành công");
            })
            .catch((err) => {
            });
    };

    const onFinish = (values) => {

        values = {
            ...values,
            hinhAnh: image ? image.name : hinhAnh,
            ngayTao, taiKhoanNguoiTao: nguoiTao.taiKhoan, maNhom, danhGia: '0', biDanh: toKebabCase(values.tenKhoaHoc), maKhoaHoc
        }

        console.log(values);
        adminServices
            .putCapNhatKhoaHoc(values)
            .then((res) => {
                if (image) {
                    UploadImage(values.biDanh);
                }
                success("cập nhật khoá học thành công")
                setTimeout(() => {
                    window.location.reload()
                }, 1000)
            })
            .catch((err) => {
                console.log(err);
            })
    };

    const onFinishFailed = (errorInfo) => {
    };

    // text area mô tả
    const { TextArea } = Input;


    // content popup
    //meseger thông báo người dùng
    const [messageApi, contextHolder] = message.useMessage();

    const error = (value) => {
        messageApi.open({
            type: 'error',
            content: value,
        });
    };

    const success = (value) => {
        messageApi.open({
            type: 'success',
            content: value,
        });
    };

    return (
        <div>
            <>
                <Button type="primary" onClick={showModal} danger>
                    Sửa
                </Button>
                <Modal title="Cập nhật khoá học" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                    <div>
                        {contextHolder}
                        <div>
                            <Form
                                name="basic"
                                labelCol={{
                                    span: 8,
                                }}
                                wrapperCol={{
                                    span: 16,
                                }}
                                style={{
                                    maxWidth: 600,
                                }}
                                initialValues={{
                                    remember: true,
                                }}
                                onFinish={onFinish}
                                onFinishFailed={onFinishFailed}
                                autoComplete="off"
                            >


                                <Form.Item
                                    label="Mã khoá học"
                                    rules={[
                                        {
                                            required: false,
                                        },
                                    ]}
                                    valuePropName={maKhoaHoc}
                                >
                                    {maKhoaHoc}
                                </Form.Item>

                                <Form.Item
                                    label="Tên khoá học"
                                    name="tenKhoaHoc"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                    ]}
                                    initialValue={tenKhoaHoc}
                                >
                                    <Input
                                        defaultValue={tenKhoaHoc}
                                    />
                                </Form.Item>

                                <Form.Item
                                    label="Lượt xem"
                                    name="luotXem"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                    ]}
                                    initialValue={luotXem}
                                >

                                    <Input
                                        defaultValue={luotXem}
                                    />
                                </Form.Item>

                                <Form.Item
                                    label="Lượt đánh giá"
                                    name="danhGia"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Nhập đánh giá!',
                                        },
                                    ]}
                                    initialValue='0'
                                >
                                    <Input
                                        defaultValue='0'
                                    />
                                </Form.Item>

                                <Form.Item
                                    label="Danh mục khoá học"
                                    name='maDanhMucKhoaHoc'
                                    initialValue={danhMucKhoaHoc.maDanhMucKhoahoc}
                                >
                                    {renderDanhMucKhoaHoc()}
                                </Form.Item>

                                <Form.Item
                                    label="Mô tả khóa học"
                                    name="moTa"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Nhập mô tả khoá học!',
                                        },
                                    ]}
                                    initialValue={moTa}
                                >
                                    <TextArea
                                        showCount
                                        style={{
                                            height: 120,
                                            resize: 'none',
                                        }}
                                        defaultValue={moTa}
                                    />
                                </Form.Item>

                                <Form.Item
                                    label="Hình ảnh"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Chọn file hình ảnh!',
                                        },
                                    ]}
                                    initialValue={hinhAnh}
                                >
                                    <Upload
                                        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                                        listType="picture"
                                        maxCount={1}
                                        fileList={fileList}
                                        beforeUpload={(file) => {
                                            setFileList()
                                            // Access file content here and do something with it
                                            setImage(file)
                                            // Prevent upload

                                            return false;
                                        }}

                                    >
                                        <Button icon={<UploadOutlined />}>Upload</Button>
                                    </Upload>
                                </Form.Item>


                                <Form.Item
                                    wrapperCol={{
                                        offset: 8,
                                        span: 16,
                                    }}
                                >
                                    <Button type="primary" htmlType="submit"
                                    >
                                        Cập nhật
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                </Modal>
            </>
        </div>
    )
}



