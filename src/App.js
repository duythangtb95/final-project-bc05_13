import "./App.css";
import HomeScreen from "./Screens/Home";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./HOC/Layout";
import LoginPage from "./Screens/LoginPage/LoginPage";
import Signup from "./Screens/Signup/Signup";
import NotPountPage from "./Screens/NotPountPage/NotPountPage";
import DetailPage from "./Screens/DetailPage/DetailPage";
import ListCategory from "./Screens/ListCategory/ListCategory";
import SearchPage from "./Screens/SearchPage/SearchPage";

// adminpage
import AdminTemlate from "./templates/AdminTemplate/AdminTemlate";
import ManageUser from "./Screens/AdminPage/ManageUser/ManageUser";
import ManageCourse from "./Screens/AdminPage/ManageCourse/ManageCourse";
// end

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomeScreen} />}></Route>
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          ></Route>
          <Route
            path="/category/:idDanhMuc"
            element={<Layout Component={ListCategory} />}
          ></Route>
          <Route
            path="/search"
            element={<Layout Component={SearchPage} />}
          ></Route>
          <Route path="/login" element={<LoginPage />}></Route>
          <Route path="/signup" element={<Signup />}></Route>

          {/* Admin */}
          <Route path="/admin/quanlynguoidung" element={<AdminTemlate Component={ManageUser} />}></Route>
          <Route path="/admin/quanlykhoahoc" element={<AdminTemlate Component={ManageCourse} />}></Route>

          {/* NotFound */}
          <Route path="*" element={<NotPountPage />}></Route>

        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
