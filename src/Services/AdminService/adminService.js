import { BASE_URL, createConfig } from "../configURL";
import axios from "axios";

export const adminServices = {
    // User
    getDanhSachNguoiDung: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01`,
            method: "GET",
            headers: createConfig(),
        });
    },
    postThemNguoiDung: (InFoAddUser) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/ThemNguoiDung`,
            method: "POST",
            data: InFoAddUser,
            headers: createConfig(),
        });
    },
    deleteXoaNguoiDung: (taiKhoanUser) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoanUser}`,
            method: "DELETE",
            headers: createConfig(),
        });
    },
    putCapNhatThongTinNguoiDung: (taiKhoanUser) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
            method: "PUT",
            data: taiKhoanUser,
            headers: createConfig(),
        });
    },
    getTimKiemNguoiDung: (value) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP01&tuKhoa=${value}`,
            method: "GET",
            headers: createConfig(),
        });
    },

    postThongTinNguoiDung: () => {
        return axios({
            url: `${BASE_URL}api/QuanLyNguoiDung/ThongTinNguoiDung`,
            method: "POST",
            headers: createConfig(),
        });
    },

    // Course
    getLayDanhSachKhoaHoc: (value) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01`,
            method: "GET",
            headers: createConfig(),

        });
    },
    getLayDanhMucKhoaHocValue: (value) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?tenKhoaHoc=${value}&MaNhom=GP01`,
            method: "GET",
            data: value,
            headers: createConfig(),
        });
    },
    getLayDanhMucKhoaHoc: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc`,
            method: "GET",
            headers: createConfig(),
        });
    },
    postThemKhoaHoc: (TTKhocHoc) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/ThemKhoaHoc`,
            method: "POST",
            data: TTKhocHoc,
            headers: createConfig(),
        });
    },
    deleteXoaKhoaHoc: (maKhoaHoc) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/XoaKhoaHoc?MaKhoaHoc=${maKhoaHoc}`,
            method: "DELETE",
            headers: createConfig(),
        });
    },
    getLayThongTinKhoaHoc: (maKhoaHoc) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${maKhoaHoc}`,
            method: "GET",
            headers: createConfig(),
        });
    },
    putCapNhatKhoaHoc: (CourseUpdate) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/CapNhatKhoaHoc`,
            method: "PUT",
            data: CourseUpdate,
            headers: createConfig(),
        });
    },
    postUploadHinhAnhKhoaHoc: (formData) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/UploadHinhAnhKhoaHoc`,
            method: "POST",
            data: formData,
            headers: createConfig(),
        });
    },

    //  ResgiterUser
    RUpostLayDanhSachKhoaHocChuaGhiDanh: (NameUser) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/LayDanhSachKhoaHocChuaGhiDanh?TaiKhoan=${NameUser}`,
            method: "POST",
            headers: createConfig(),
        });
    },
    RUpostLayDanhSachKhoaHocDaXetDuyet: (taikhoanUser) => {
        return axios({
            url: `${BASE_URL}api/QuanLyNguoiDung/LayDanhSachKhoaHocDaXetDuyet`,
            method: "POST",
            data: taikhoanUser,
            headers: createConfig(),
        });
    },
    RUpostLayDanhSachKhoaHocChoXetDuyet: (taikhoanUser) => {
        return axios({
            url: `${BASE_URL}api/QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet`,
            method: "POST",
            data: taikhoanUser,
            headers: createConfig(),
        });
    },
    RUpostHuyGhiDanh: (taiKhoan, maKhoaHoc) => {
        let Data = {
            taiKhoan,
            maKhoaHoc,
        }
        return axios({
            url: `${BASE_URL}api/QuanLyKhoaHoc/HuyGhiDanh`,
            method: "POST",
            data: Data,
            headers: createConfig(),
        });
    },
    RUpostGhiDanhKhoaHoc: (taiKhoan, maKhoaHoc) => {
        let Data = {
            taiKhoan,
            maKhoaHoc,
        }
        return axios({
            url: `${BASE_URL}api/QuanLyKhoaHoc/GhiDanhKhoaHoc`,
            method: "POST",
            data: Data,
            headers: createConfig(),
        });
    },

    // ResgiterCourse
    RCpostLayDanhSachNguoiDungChuaGhiDanh: (maKhoaHoc) => {
        return axios({
            url: `${BASE_URL}api/QuanLyNguoiDung/LayDanhSachNguoiDungChuaGhiDanh`,
            method: "POST",
            data: maKhoaHoc,
            headers: createConfig(),
        });
    },
    RCpostLayDanhSachHocVienKhoaHoc: (maKhoaHoc) => {
        return axios({
            url: `${BASE_URL}api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc`,
            method: "POST",
            data: maKhoaHoc,
            headers: createConfig(),
        });
    },
    RCpostLayDanhSachHocVienChoXetDuyet: (maKhoaHoc) => {
        return axios({
            url: `${BASE_URL}api/QuanLyNguoiDung/LayDanhSachHocVienChoXetDuyet`,
            method: "POST",
            data: maKhoaHoc,
            headers: createConfig(),
        });
    },
    RCpostGhiDanhKhoaHoc: (taiKhoan, maKhoaHoc) => {
        let Data = {
            taiKhoan,
            maKhoaHoc,
        }
        return axios({
            url: `${BASE_URL}api/QuanLyKhoaHoc/GhiDanhKhoaHoc`,
            method: "POST",
            data: Data,
            headers: createConfig(),
        });
    },
    RCpostHuyGhiDanh: (taiKhoan, maKhoaHoc) => {
        let Data = {
            taiKhoan,
            maKhoaHoc,
        }
        return axios({
            url: `${BASE_URL}api/QuanLyKhoaHoc/HuyGhiDanh`,
            method: "POST",
            data: Data,
            headers: createConfig(),
        });
    },

}



