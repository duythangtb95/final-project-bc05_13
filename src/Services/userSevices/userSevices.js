import { BASE_URL, createConfig } from "../configURL";
import axios from "axios";
export const userSevices = {
  postDangNhap: (dataUser) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: dataUser,
      headers: createConfig(),
    });
  },
  postDangKy: (dataUser) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
      method: "POST",
      data: dataUser,
      headers: createConfig(),
    });
  },
};
